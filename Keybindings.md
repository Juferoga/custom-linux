# **Keybindings**

Algunos atajos de teclado definidos en el entorno awesome.<br>


**Modkey = Tecla Inicio**


Mostrar Atajos:<br>
<code>**Modkey** + F1</code>


Desplazarse entre los espacios de trabajo:<br>
<code>**Control** + **Alt** + ↑</code><br>
<code>**Control** + **Alt** + ↓</code>


Desplazarse entre pestañas:<br>
<code>**Modkey** + →</code><br>
<code>**Modkey** + ←</code>


Abrir el panel lateral:<br>
<code>**Modkey** + R</code>


Desplazarse entre ventanas:<br>
<code>**Alt** + **Tab**</code><br>
<code>**Alt** + **Shift** + **Tab**</code>


Bloquear pantalla:<br>
<code>**Modkey** + L</code>


Abrir el editor de texto:<br>
<code>**Modkey** + C</code>


Abrir el navegador:<br>
<code>**Modkey** + B</code>


Abrir una terminal:<br>
<code>**Modkey** + X</code>


Reiniciar el entorno awesome:<br>
<code>**Modkey** + **Control** + R</code>


Cerrar sesión en awesome:<br>
<code>**Modkey** + **Control** + Q</code>


Control de ancho y alto de las ventanas:<br>
<code>**Alt** + **Shift** + ←↑↓→</code>
