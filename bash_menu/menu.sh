#! /bin/bash
cmd=(dialog --separate-output --checklist "¡Hola! ¿A qué proyecto en pantalla pertences?" 22 76 16)
options=(1 "Ing. Catastral y Geodesia" off
         2 "Ing. de Sistemas" off
         3 "Ing. Agricola" off
         4 "Ing. Electronica" off
         5 "Ing. en Control y Automatización" off
         6 "Salir" off)
choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)

clear

for choice in $choices; do
    printf " [*] $choice. "
    case $choice in
        1)
            printf "Seleccionaste Ing. Catastral y Geodesia\n"
            bash ./per_carrers/catastral.sh
            ;;
        2)
            printf "Seleccionaste Ing. de Sistemas\n"
            bash ./per_carrers/Sistemas.sh
            ;;
        3)
            printf "Seleccionaste Ing. Agricola\n"
            bash ./per_carrers/Agricola.sh
            ;;
        4)
            printf "Seleccionaste Ing. Electronica\n"
            bash ./per_carrers/Electronic.sh
            ;;
        5)
            printf "Seleccionaste Ing. en Control y Automatización\n"
            bash ./per_carrers/Control_and_Automation.sh
            ;;
        6)
            echo "Salir"
            ;;
    esac
done
