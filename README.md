# **Personalización entorno de escritorio awesome - GNUBIES 2020**

<img src="https://www.udistrital.edu.co/themes/custom/versh/images/default/preloader.png" align="left" width="192px" height="192px"/>
<img align="left" width="0" height="240px" hspace="10"/>

>Brian Stiffenn Luna Bolívar

>Raúl Camilo Martín Bernal

>Johan Alexis Castelblanco Vela

>Antony Eugene Castelblanco Vela

>Braian Lara Hidalgo



**Instructor** Juan Felipe Rodriguez. 

En el siguiente documento se muestra la propuesta de desarrollo de un DE, mediante el cual se pretende dar a conocer los alcances y limitaciones (si es el caso) del software libre a comparación del software privativo.

# *Tareas (Viernes 24 ** 1er Avance Grupal)*

- Hacer una funcion de instalacion de paquetes por carrera (ALL except Brian L.) ✅
- Hacer Menu (Brian Luna)✅
- Hacer funcion sistemas (Instructor)
- Cambio de Diseño (Johan Castelblanco)✅

# *Tareas (14 ** 2do Avance Grupal)*

- Cambiar color barras (Johan Casteblanco) 
- Modulacion de Bash (Antony)    -> llamar bash desde bash     ex.  sh ./bash2.sh          or      /bin/bash/ 
- Cambialcolor de widgets -Rofi- (Brian Lara) 
- Instalar Grub custom (Brian luna) 
- Construir un .bashrc (Raul M.) (Usar alias de linux para automatizar tareas)

_____________Versionamiento con git______________

# *Tareas 31

- Documento (Google docs) -> pdf
- Presentación (Xdada uno) Explicar que hizo -> pdf
- Exportar Release1 -> Primera Versión.

# Indice

1) Instalar el entorno de escritorio

 ... ( to be continued ... )

1) Para instalar el entorno de escritorio debemos correr el archivo install.sh

<code>$ chmod +x install.sh</code><br>
<code>$ sudo ./install.sh</code>

O tambien se puede ejecutar

<code>$ /bin/bash -c "$(curl -fsSL https://gitlab.com/Juferoga/custom-linux/-/raw/master/install.sh)"</code>

ToDo ....

No se instala con el bash en arch pero si en debian.... revisar
Para la instalcion de las dependencias de arch ejecutar

<code>sudo pacman -S --noconfirm awesome rofi compton i3lock xclip mate-polkit qt5-styles-gtk2 materia-theme lxappearance xbacklight flameshot git</code>
