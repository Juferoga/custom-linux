#!/bin/bash

echo "Se recomienda instalar la fuente roboto en el sistema operativo"
echo "Debian : fonts-roboto  | Arch : ttf-roboto"
echo "Empezando la instalacion de Awesome y sus dependencias"

function has_command(){
  command -v $1 > /dev/null
}

install_awesome() {
  if [ ! "$(which awesome 2> /dev/null)" ]; then
    printf "\n 'awesome' necesita ser instalado\n"
    if has_command zypper; then
      sudo zypper in awesome
    elif has_command apt-get; then
      sudo apt-get install git awesome rofi compton i3lock-fancy xclip gnome-keyring policykit-1-gnome materia-gtk-theme lxappearance
    elif has_command dnf; then
      sudo dnf install -y awesome
    elif has_command yum; then
      sudo yum install awesome
    elif has_command pacman; then
	# some packages dont appear and could be generate conflicts, add ranger for faster move in terminal :) 
      sudo pacman -S git awesome rofi compton i3lock xclip mate-polkit materia-theme lxappearance flameshot ranger
    fi
  fi
}
install_awesome

echo "Instalando el tema light en español"

git clone https://gitlab.com/Juferoga/custom-linux.git ~/.config/awesome

echo "clonado realizado, se cerrara la sesion para comprobar la instalación"

sleep 30

sudo pkill -KILL -u $USER

