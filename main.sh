#!/bin/bash
clear
#Declaración de colores
Red=$'\e[1;31m'
Green=$'\e[1;32m'
Blue=$'\e[1;34m'

printf "$Green
       _____ _   _ _    _ _     _             ___   ___ ___   ___  
      / ____| \ | | |  | | |   (_)           |__ \ / _ \__ \ / _ \ 
     | |  __|  \| | |  | | |__  _  ___  ___     ) | | | | ) | | | |
     | | |_ | . ' | |  | | '_ \| |/ _ \/ __|   / /| | | |/ /| | | |
     | |__| | |\  | |__| | |_) | |  __/\__ \  / /_| |_| / /_| |_| |
      \_____|_| \_|\____/|_.__/|_|\___||___/ |____|\___/____|\___/ 
                                                               
"

printf "$Blue
 --------------------------------------------------------------------------
 |  Bienvenid@, con este proyecto puedes personalizar tu distro de Linux  |
 |  con un escritorio customizado y las aplicaciones que desees según     |
 |  tu carrera                                                            |
 --------------------------------------------------------------------------
\n"

read -n 1 -s -r -p "Presiona una tela para continuar"

f1=0
while [ $f1 -eq 0 ]; do 
    # Menú diálogo checklist para selecionar opciones
        # Dejo la opción de elegir software de la carrera de primeras y personalizar comandos alias, ya que 
        # es son las opciones que requieren que el usuario ingrse datos o confirme las opciones que desea. 
        # La idea es que se eligan todas las configuraciones primero,
        # y luego el Script se encarge del resto sin esperar más datos de ingreso
        # La instalación del Custom DE la dejo de últimas porque es la que requiere cerrar sesión
    cmd=(dialog --separate-output --checklist "Selecciona las opciones que deseas con barra espaciadora:" 22 76 16)
    options=(1 "Selecionar e instalar software de tu carrera" off
             2 "Personalizar comandos con alias bashrc" off
             3 "Instalar GRUB personalizado" off
             4 "Instalar entorno de escritorio personalizado" off
             5 "Salir" off)
    choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
    clear
    echo ""
    for choice in $choices; do
        printf "$Blue [*] $choice. "
        case $choice in
            1)
                echo "Se te preguntará tu carrera y los programas que deseas instalar"
                ;;
            2)
                echo "Podrás personalizar comandos con un alias"
                ;;
            3)
                echo "Se instalará el GRUB personalizado"
                ;;
            4)
                echo "Se instalará el entorno de escritorio personalizado y sus dependencias"
                ;;
            5)
                echo "Salir"
                ;;
        esac
    done
    f2=0
    while [ $f2 -eq 0 ]; do
        echo "$Green"
        read -p "Ingresa 'S' si estas de acuerdo, 'N' en caso contrario, pueden ser minúsculas: " answer
        answer=${answer^^}
        echo $answer
        case $answer in
        [S])
            f2=1
            f1=1
            ;;
        [N])
            f2=1
            f1=0
            ;;
        *)
            printf "\nOpción incorrecta, intenta de nuevo con 'S' o 'N'"
            f2=0
            ;;
        esac
    done
done
# 1.
    # chmod +x ./install.sh
    # ./install.sh
# 2.
    #bash ./install.sh
if [ $f1 -eq 1 ]; then
    for choice in $choices; do
        printf "$Blue [*] $choice. "
        case $choice in
            1)
                printf "Menú de carreras...\n\n"
                bash ./bash_menu/menu.sh
                ;;
            2)
                printf "Iniciando personalización de comandos con un alias... \n\n"
                bash ./extraOptions/editBashAlias.sh
                ;;
            3)
                printf "Instalando el GRUB personalizado...\n\n"
                bash ./extraOptions/installCustomGrub.sh
                ;;
            4)
                printf "Installing Custom Desktop Environment...\n"
                bash ./installCustomDE.sh                
                ;;
            5)
                echo "Salir"
                ;;
        esac
    done
fi
