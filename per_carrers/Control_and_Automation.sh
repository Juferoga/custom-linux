install_control() {
  if [ ! "$(which arduino 2> /dev/null)" ]; then
    echo "\n Arduino necesita instalarse" 
    #prompt -i "\n 'arduino' necesita ser instalado"
    if has_command zypper; then
      sudo zypper in arduino
    elif has_command apt-get; then
      sudo apt-get install arduino 
	  sudo apt-add-repository ppa:qucs/qucs
      sudo apt-get update
      sudo apt-get install quc
    elif has_command dnf; then
      sudo dnf install -y arduino
    elif has_command yum; then
      sudo yum install arduino
    elif has_command pacman; then
      sudo pacman -S arduino
	  git clone https://aur.archlinux.org/snapd.git
      cd snapd
      makepkg -si
	  sudo systemctl enable --now snapd.socket
	  sudo ln -s /var/lib/snapd/snap /snap
	  sudo snap install qucs-spice

    fi
  fi
}
install_control
