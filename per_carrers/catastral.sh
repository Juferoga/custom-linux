#!/bin/bash


installQgis(){
	if has_command zypper; then
	 echo "por el momento no disponible"
	elif has_command apt-get; then
	wget -qO - https://qgis.org/downloads/qgis-2020.gpg.key | sudo gpg --no-default-keyring --keyring gnupg-ring:/etc/apt/trusted.gpg.d/qgis-archive.gpg --import
	sudo add-apt-repository "deb https://qgis.org/debian `lsb_release -c -s` main"
	sudo apt update
	sudo apt install qgis qgis-plugin-grass
	elif has_command dnf; then
	 echo "por el momento no disponible"
	elif has_command yum; then
	 echo "por el momento no disponible"
	elif has_command pacman; then
	 sudo pacman -S qgis
	fi
}
installRstudio(){
	if has_command zypper; then
	 echo "por el momento no disponible"
	elif has_command apt-get; then
	 sudo apt -y install wget
	 wget https://download1.rstudio.org/desktop/bionic/amd64/rstudio-1.2.5042-amd64.deb
	 sudo apt install ./rstudio-1.2.5042-amd64.deb 
	elif has_command dnf; then
	 echo "por el momento no disponible"
	elif has_command yum; then
	 echo "por el momento no disponible"
	elif has_command pacman; then
	 git clone https://aur.archlinux.org/yay.git
	 cd yay
	 makepkg -si
	 yay -S rstudio-desktop-bin
	 yay -S openblas-lapack
	 
	fi
}
installR() {
	if has_command zypper; then
	 echo "por el momento no disponible"
	elif has_command apt-get; then
	 sudo apt install dirmngr gnupg apt-transport-https ca-certificates software-properties-common
	 sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
     sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu focal-cran40/'
	elif has_command dnf; then
	 echo "por el momento no disponible"
	elif has_command yum; then
	 echo "por el momento no disponible"
	elif has_command pacman; then
	 pacman -S r
	 while true
	  do
	   read -r -p "Para muchos paquetes de R necesitara compilar fortran es recomenda instalar gcc-fortran ¿Desea instalarlo? " input
		 case $input in
			 [yY][eE][sS]|[yY])
				 echo "Instalando gcc-fortran"
				 sudo pacman -S gcc-fortran
				 break
				 ;;
			 [nN][oO]|[nN])
				 echo "Instalacion de gcc-fortran omitida"
				 break
				 ;;
			 *)
				 echo "Inserte un comando valido"
				 ;;
	  esac
	 done
	fi
}

catastral() {

	echo "¿Eres catastral?, muy bien, cuantas veces te han preguntado ¿Por que estudiaste catastral?"
	read timesasked
	timesme=$((timesasked+2))
	echo Genial solo te preguntaron $timesasked a mi me preguntaron $timesasked+2 veces, bueno ahora sigamos con los paquetes recomendados:
	# instalar Qgis
	while true
	do
	 read -r -p "Desea instalar Qgis? ((Y) o n): " input
		 case $input in
			 [yY][eE][sS]|[yY])
				 echo "Instalando Qgis"
				 installQgis
				 break
				 ;;
			 [nN][oO]|[nN])
				 echo "Instalacion de Qgis omitida"
				 break
				 ;;
			 *)
				 echo "Inserte un comando valido"
				 ;;
	 esac
	done
	# instalar 	R
	while true
	do
	 read -r -p "Desea instalar R? ((Y) o n): " input
		 case $input in
			 [yY][eE][sS]|[yY])
				 echo "Instalando R"
				 installR
				 # instalar Rstudio
				while true
				do
				read -r -p "Desea instalar Rstudio? ((Y) o n): " input
					case $input in
						[yY][eE][sS]|[yY])
							echo "Instalando Rstudio"
							installRstudio
							break
							;;
						[nN][oO]|[nN])
							echo "Instalacion de Rstudio omitida"
							break
							;;
						*)
							echo "Inserte un comando valido"
							;;
				esac
				done
				 break
				 ;;
			 [nN][oO]|[nN])
				 echo "Instalacion de R omitida"
				 break
				 ;;
			 *)
				 echo "Inserte un comando valido"
				 ;;
	 esac
	done
	
}

catastral

