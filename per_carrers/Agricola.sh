function has_command(){
  command -v $1 > /dev/null
}


freecad(){
    if has_command apt-get; then
        sudo apt-get install -y freecad

    elif has_command zypper; then
        sudo zypper install freecad

	elif has_command dnf; then
        sudo dnf install -y freecad

	elif has_command yum; then
        sudo yum install freecad

	elif has_command pacman; then
        sudo pacman -S --noconfirm freecad

	fi
}

gimp(){
    if has_command apt-get; then
        sudo apt-get install -y gimp

    elif has_command zypper; then
        sudo zypper install gimp

	elif has_command dnf; then
        sudo dnf install -y gimp

	elif has_command yum; then
        sudo yum install gimp

	elif has_command pacman; then
        sudo pacman -S --noconfirm gimp

	fi
}

audacity(){
    if has_command apt-get; then
        sudo apt-get install -y audacity

    elif has_command zypper; then
        echo "No disponible"

	elif has_command dnf; then
        echo "No disponible"

	elif has_command yum; then
        echo "No disponible"

	elif has_command pacman; then
        echo "No disponible"

	fi
}

vlc(){
if has_command apt-get; then
        sudo apt-get install -y vlc

    elif has_command zypper; then
        sudo zypper ar https://download.videolan.org/pub/vlc/SuSE/<SUSE version> VLC
        sudo zypper mr -r VLC
        sudo zypper in vlc

	elif has_command dnf; then
        sudo dnf install -y vlc

	elif has_command yum; then
        sudo yum localinstall --nogpgcheck https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
        sudo yum install vlc

	elif has_command pacman; then
        sudo pacman -S --noconfirm vlc

	fi
}

sevenZip(){
    if has_command apt-get; then
        sudo apt-get install -y p7zip

    elif has_command zypper; then
        sudo zypper install p7zip

	elif has_command dnf; then
        sudo dnf install -y p7zip

	elif has_command yum; then
        sudo yum install -y -q p7zip p7zip-plugins

	elif has_command pacman; then
        sudo pacman -S --noconfirm p7zip

	fi
}

selectApps(){
    case $1 in
      0)
        printf "\n\nInstalando FreeCAD\n"
        freecad
      ;;
      1)
        printf "\n\nInstalando GIMP\n"
        gimp
      ;;
      2)
        printf "\n\nInstalando Audacity\n"
        audacity
      ;;
      3)
        printf "\n\nInstalando VLC\n"
        vlc
      ;;
      4)
        printf "\n\nInstalando 7zip\n"
        sevenZip
      ;;
    esac
}



Agricola(){
    printf "\n\nHas elegido Ingeniería Agrícola.\nA continuación puedes elegir que software instalar: "

    packages=("FreeCAD" "GIMP" "Audacity" "VLC" "7zip")
    j=0
    f1=0
    printf "Ingresa 'S' para Si o 'N' para No.\n\n"
    
    for i in ${packages[*]}; do     
        while [ $f1 -eq 0 ]; do     
            read -p "Deseas descargar/instalar $i S/N: " answers[$j]
            answers[$j]=${answers[$j]^^}
            if [ "${answers[j]}" = "S" ] || [ "${answers[j]}" = "N" ]; then
                f1=1
            else
                echo "Opción incorrecta, intenta de nuevo"
                f1=0
            fi
        done
        j=$(($j+1))
        f1=0
    done
    j=0
    f1=0
    for i in ${answers[*]}; do
        if [ $i = "S" ]; then
            selectApps $j
        fi
        j=$(($j+1))
    done    
}

Agricola




    
