#!/bin/bash
function has_command(){
  command -v $1 > /dev/null
}

verifyPackageManager(){
    if has_command apt-get; then
        packageManager="apt-get install -y"
    elif has_command pacman; then
        packageManager="pacman -S --noconfirm"
    elif has_command zypper; then
        packageManager="zypper in"
    elif has_command dnf; then
        packageManager="dnf install -y"
    elif has_command yum; then
        packageManager="yum install"
    fi
}
electronicApps(){
    echo "Seleccionaste que estudias electrónica"
    echo "Muy bien, elige los paquetes de software que deseas descargar/instalar:"    
    packages=("KiCad" "MyOpenLab" "Arduino" "Analizador_de_señales")
    j=0
    f1=0
    printf "Ingresa 'S' para Si o 'N' para No. Descuida también pueden ser minúsculas\n\n"
    for i in ${packages[*]}; do     
        while [ $f1 -eq 0 ]; do     
            read -p "Deseas descargar/instalar $i S/N: " answers[$j]
            answers[$j]=${answers[$j]^^}
            if [ "${answers[j]}" = "S" ] || [ "${answers[j]}" = "N" ]; then
                f1=1
            else
                echo "Opción incorrecta, intenta de nuevo"
                f1=0
            fi
        done
        j=$(($j+1))
        f1=0
    done
    j=0
    f1=0
    for i in ${answers[*]}; do
        if [ $i = "S" ]; then
            selectApps $j
        fi
        j=$(($j+1))
    done
}
selectApps(){
    verifyPackageManager
    case $1 in
      0)
        echo "Instalando KiKad"
        sudo  $packageManager kicad
      ;;
      1)
        echo "Descargando MyOpenLab"
        wget -P /home/$USER/Descargas https://myopenlab.org/distribution_linux_3.11.0.zip
        echo "Descomprimiendo MyOpenLab en Escritorio"
        unzip  /home/$USER/Descargas/distribution_linux_3.11.0.zip -d /home/$USER/Escritorio/MyOpenLab
        chmod +x /home/$USER/Escritorio/MyOpenLab/start_linux
        echo "Recuerda tener Java en versión 8 para ejecutarlo"
      ;;
      2)
        arq=$(uname -m) #Para verificar la arquitetura de la máquina
        if [ $arq = "i686" ] || [ $arq = "i386" ]; then
            arduino="arduino-1.8.13-linux32.tar.xz"
        elif [ $arq = "x86_64" ]; then
            arduino="arduino-1.8.13-linux64.tar.xz"
        fi
        echo "Descargando Arduino"
        wget -P /home/$USER/Descargas https://downloads.arduino.cc/$arduino
        echo "Descomprimiendo Arduino en Escritorio"
        mkdir /home/$USER/Escritorio/Arduino
        tar -xf /home/$USER/Descargas/$arduino -C /home/$USER/Escritorio/Arduino
        chmod +x /home/$USER/Escritorio/Arduino/arduino-1.8.13/install.sh
        echo "Instalando Arduino"
        /home/$USER/Escritorio/Arduino/arduino-1.8.13/./install.sh
      ;;
      3)
        echo "Instalando analizador de señales"
        sudo $packageManager sigrok-cli pulseview
      ;;
    esac
}
electronicApps
